using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RecipeView : MonoBehaviour
{
    [SerializeField] Image Highlight;
    [SerializeField] Text TitleText;

    // Icons
    [SerializeField] GameObject Burger;
    [SerializeField] GameObject Cheese;
    [SerializeField] GameObject Lettuce;
    [SerializeField] GameObject Tomato;

    // Quantities
    [SerializeField] Text BurgerText;
    [SerializeField] Text CheeseText;
    [SerializeField] Text LettuceText;
    [SerializeField] Text TomatoText;

    // earnings
    [SerializeField] Text MoneyEarnedText;
    [SerializeField] Text TimeEarnedText;

    public void PopulateFor(Recipe recipe)
    {
        TitleText.text = recipe.Name;

        // update each ingedient to show it or not
        var burgerCount = recipe.Ingedients.Where(i => i == IngredientType.Burger).Count();
        Burger.SetActive(burgerCount > 0);
        BurgerText.text = $"x{burgerCount}";

        var cheeseCount = recipe.Ingedients.Where(i => i == IngredientType.Cheese).Count();
        Cheese.SetActive(cheeseCount > 0);
        CheeseText.text = $"x{cheeseCount}";

        var lettuceCount = recipe.Ingedients.Where(i => i == IngredientType.Lettuce).Count();
        Lettuce.SetActive(lettuceCount > 0);
        LettuceText.text = $"x{lettuceCount}";

        var tomatoCount = recipe.Ingedients.Where(i => i == IngredientType.Tomato).Count();
        Tomato.SetActive(tomatoCount > 0);
        TomatoText.text = $"x{tomatoCount}";

        // money and time
        MoneyEarnedText.text = $"{recipe.MoneyEarned:C}";
        TimeEarnedText.text = $"+{recipe.TimeEarned} Sec";

        SetCurrent(false);
    }

    public void SetCurrent(bool isCurrent)
    {
        Highlight.enabled = isCurrent;
    }
}
