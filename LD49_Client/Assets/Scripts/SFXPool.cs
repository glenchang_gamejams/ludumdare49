using System.Collections.Generic;
using UnityEngine;

public class SFXPool : MonoBehaviour
{
    [SerializeField] AudioSource AudioSource;
    [SerializeField] List<AudioClip> Clips;

    public void Play()
    {
        if (Clips.Count == 0)
            return;

        var index = Random.Range(0, Clips.Count);
        AudioSource.PlayOneShot(Clips[index]);
    }
}
