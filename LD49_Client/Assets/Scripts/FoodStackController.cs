using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodStackController : MonoBehaviour
{
    [SerializeField] public Transform Base;
    [SerializeField] public AnimationCurve Rubberband;
    [SerializeField] AudioSource ImpactSFX;

    List<Transform> Transforms = new List<Transform>();
    Transform LastTransform;

    [SerializeField] float BounceFrequency = 1;
    [SerializeField] float BounceAmplitude = 1;
    [SerializeField] float BounceAdd = 1;
    float BounceStrength = 0;
    bool Moving = false;
    float StartMoveTime;
    float StopMoveTime;
    

    public List<IngredientType> GetIngedients()
    {
        List<IngredientType> ingredients = new List<IngredientType>();
        foreach (var t in Transforms)
        {
            var ingredient = t.GetComponent<IngredientController>();
            if (ingredient != null)
                ingredients.Add(ingredient.IngredientType);
        }
        return ingredients;
    }

    public void ClearStack()
    {
        while (Transforms.Count > 0)
        {
            var t = Transforms[0];
            Transforms.RemoveAt(0);
            Destroy(t.gameObject);
        }
        LastTransform = Base;
    }

    public float Height => transform.position.y;

    private void Update()
    {
        bool moving = Mathf.Abs(Input.GetAxis("Horizontal")) > .1f || Mathf.Abs(Input.GetAxis("Vertical")) > .1f;
        if (Moving != moving)
        {
            if (moving)
                StartMoveTime = Time.time;
            else
                StopMoveTime = Time.time;
            Moving = moving;
        }
        if (Moving)
        {
            BounceStrength = Mathf.Min(1, (Time.time - StartMoveTime));
        }
        else
        {
            BounceStrength = Mathf.Max(0, BounceStrength + StopMoveTime - Time.time);
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        UpdatePosition();
        UpdateFoodPositions();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (Transforms.Contains(collision.gameObject.transform))
            return;

        var rigidBody = collision.gameObject.GetComponent<Rigidbody>();
        rigidBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        rigidBody.isKinematic = true;
        AddTransform(collision.gameObject.transform);

        if (ImpactSFX != null)
        {
            ImpactSFX.pitch = Random.Range(0.7f, 1);
            ImpactSFX.Play();
        }
    }

    private void AddTransform(Transform t)
    {
        Transforms.Add(t);
        LastTransform = t;
    }

    private void UpdatePosition()
    {
        if (LastTransform == null)
            LastTransform = Base;
        transform.position = LastTransform.position + new Vector3(0, .25f, 0);
    }

    private void UpdateFoodPositions()
    {
        Transform previous = Base;
        int i = 0;
        int breakIndex = -1;
        foreach (var t in Transforms)
        {
            if (breakIndex < 0 && (new Vector2(previous.position.x, previous.position.z) - new Vector2(t.position.x, t.position.z)).sqrMagnitude > .4f)
            {
                breakIndex = i;
                t.gameObject.layer = Layers.Garbage;
                LastTransform = previous;
            }
            if(breakIndex >= 0)
            {
                t.GetComponent<Rigidbody>().isKinematic = false;
                continue;
            }
            var targetForward = t.forward + .1f * (new Vector3(t.forward.x, 0, t.forward.z).normalized - t.forward);
            //var targetUp = t.up + .1f * (new Vector3(0, -1, 0) - t.up);
            t.LookAt(t.position + targetForward, new Vector3(0,-1,0));
            var lastComponentHeight = .2f;
            var lastRenderer = previous.GetComponent<Renderer>();
            if (lastRenderer != null)
                lastComponentHeight = 2*lastRenderer.bounds.extents.y;

            t.position += (previous.position + new Vector3(0, lastComponentHeight, 0) - t.position) * Rubberband.Evaluate(i);
            previous = t;
            ++i;

            t.position += new Vector3(0, BounceEvaluate(Time.fixedTime + i * BounceAdd));
        }

        if(breakIndex >= 0)
        {
            Transforms.RemoveRange(breakIndex, Transforms.Count - breakIndex);
        }
    }

    private float BounceEvaluate(float time)
    {
        return (Mathf.Sin(BounceFrequency*time)+1)*BounceAmplitude*BounceStrength;
    }
}
