using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoardUIController : MonoBehaviour
{
    [SerializeField]
    private Text m_nameListText;

    [SerializeField]
    private Text m_scoreListText;

    [SerializeField]
    private Text m_lastScoreLabel;

    [SerializeField]
    private Text m_lastScoreText;

    [SerializeField]
    private Text m_rankText;

    [SerializeField]
    private GameObject m_fetchingScoresText;

    private void OnEnable()
    {
        // Fetch Scores
        LeaderBoards.Instance?.GetTopScores();
        LeaderBoards.Instance?.GetRank();

        m_nameListText.gameObject.SetActive(false);
        m_scoreListText.gameObject.SetActive(false);
        m_fetchingScoresText.gameObject.SetActive(true);

        m_lastScoreLabel.text = $"{LeaderBoards.Instance?.Name.ToString()} Last Score:  ";
        m_lastScoreText.text = LeaderBoards.Instance?.Score.ToString();
        m_rankText.text = LeaderBoards.Instance?.Rank.ToString();

        EventManager.AddListener<TopScoreRecievedEvent>(OnTopScoreRecievedEvent);
        EventManager.AddListener<ScoreChangedEvent>(OnScoreChangedEvent);
        EventManager.AddListener<RankRecievedEvent>(OnRankRecievedEvent);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<TopScoreRecievedEvent>(OnTopScoreRecievedEvent);
        EventManager.RemoveListener<ScoreChangedEvent>(OnScoreChangedEvent);
        EventManager.RemoveListener<RankRecievedEvent>(OnRankRecievedEvent);
    }

    private void OnScoreChangedEvent(ScoreChangedEvent evt)
    {
        m_lastScoreText.text = evt.Score.ToString();
    }

    private void OnRankRecievedEvent(RankRecievedEvent evt)
    {
        m_rankText.text = evt.Rank.ToString();
    }

    /// <summary>
    ///   When our top scores are retrieved show results
    /// </summary>
    private void OnTopScoreRecievedEvent(TopScoreRecievedEvent evt)
    {
        m_nameListText.gameObject.SetActive(true);
        m_scoreListText.gameObject.SetActive(true);
        m_fetchingScoresText.gameObject.SetActive(false);

        string names = "";
        string scores = "";
        foreach (var scoreRecord in evt.TopScores)
        {
            names = names.ConcatString($"{scoreRecord.PlayerName}\n");
            scores = scores.ConcatString($"{scoreRecord.Score}\n");
        }

        m_nameListText.text = names;
        m_scoreListText.text = scores;
        m_lastScoreText.text = LeaderBoards.Instance?.Score.ToString();

        if(LeaderBoards.Instance?.Rank != 0)
            m_rankText.text = LeaderBoards.Instance.Rank.ToString();
    }
}
