/*
Refer to https://gamedevelopment.tutsplus.com/tutorials/how-to-code-a-self-hosted-phpsql-leaderboard-for-your-game--gamedev-11627

// Creating the table
CREATE TABLE Scores(
    name VARCHAR(10) NOT NULL DEFAULT 'Anonymous' PRIMARY KEY,
    score INT(5) UNSIGNED NOT NULL DEFAULT '0',
    ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)
ENGINE=InnoDB; 
 */


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct ScoreRecord
{
    public string PlayerName;
    public string Score;
    public string Rank;

    public ScoreRecord(string playerName, string score, string rank)
    {
        PlayerName = playerName;
        Score = score;
        Rank = rank;
    }
}

public struct TotalScoreRecord
{
    public string PlayerName;
    public string Score;
    public string Rank;

    public TotalScoreRecord(string playerName, string score, string rank)
    {
        PlayerName = playerName;
        Score = score;
        Rank = rank;
    }
}


public class LeaderBoards : MonoBehaviour
{
    public static LeaderBoards Instance = null;

    public Dictionary<int, int> LevelScores = new Dictionary<int, int>();

    private string m_playerName = "NoName";
    private int m_playerScore = 0;
    private int m_playerRank = 0;

    public string Name => m_playerName;
    public int Score => m_playerScore;
    public int Rank => m_playerRank;

    // Database
    private string m_database = "gchang_game_monsterburger";
    private string m_privateKey = "gbc052510180109";

    // External references
    private string m_topScoresURL = "https://www.glenchang.com/main/games/MonsterBurger/TopScores.php?";
    private string m_addScoreURL = "https://www.glenchang.com/main/games/MonsterBurger/AddScore.php?";
    private string m_rankURL = "https://www.glenchang.com/main/games/MonsterBurger/GetRank.php?"; 

    // For use for games with multiple levels where score records are kept per level.
    //private string m_levelsCompleted = "https://www.glenchang.com/main/games/MonsterBurger/LevelsCompleted.php?";
    //private string m_totalTopScoresURL = "http://www.glenchang.com/main/games/MonsterBurger/TotalScores.php?";
    //private string m_totalAddScoreURL = "http://www.glenchang.com/main/games/MonsterBurger/AddTotalScore.php?";
    //private string m_totalRankURL = "http://www.glenchang.com/main/games/MonsterBurger/GetTotalRank.php?"; 

    public void AddScore(int level=0, int score=0)
    {
        StartCoroutine(AddScoreRoutine(level, score));
    }

    // For use for games with multiple levels where score records are kept per level.
    //public void GetLevelScores()
    //{
    //    StartCoroutine(GetLevelScoresRoutine());
    //}

    public void TestAddScore()
    {
        StartCoroutine(AddScoreRoutine(0, Random.Range(0, 100)));
    }

    public void TestGetScores()
    {
        StartCoroutine(GetTopScoresRoutine(0));
    }

    public void TestRank()
    {
        StartCoroutine(GetRankRoutine(0));
    }

    public void GetRank()
    {
        StartCoroutine(GetRankRoutine()); // Get our Rank
    }

    public void GetTopScores()
    {
        StartCoroutine(GetTopScoresRoutine()); // Get our top scores
    }

    // For use for games with multiple levels where score records are kept per level.
    //public void GetTopTotalScores()
    //{
    //    StartCoroutine(GetTotalTopScoresRoutine()); // Get our top scores
    //    StartCoroutine(GrabTotalRankRoutine());
    //}

    ///Our encryption function: http://wiki.unity3d.com/index.php?title=MD5
    private string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

#region Coroutines
    private IEnumerator AddScoreRoutine(int level=0, int score=0)
    {
        Debug.Log($"Add Scores Url is {m_addScoreURL}");
        string hash = Md5Sum(m_playerName + score + m_privateKey);
        Debug.Log(m_addScoreURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(m_playerName), "&level=", level, "&score=", score, "&hash=", hash));
        WWW ScorePost = new WWW(m_addScoreURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(m_playerName), "&level=", level, "&score=", score, "&hash=", hash));
        yield return ScorePost; // The function halts until the score is posted.
        if (ScorePost.error == null)
        {
            Debug.Log("Success in sending add score");

            //StartCoroutine(GetTopScoresRoutine(level)); // Get our top scores for the level
            //StartCoroutine(GrabRank( GameManager.Instance.PlayerName, UIManager.Instance.m_requestingLevelResults));

            // Also determine and set total score for games with multiple levels
            // StartCoroutine(DetermineTotalScoreRoutine());
        }
        else
        {
            Debug.Log("Failed to add score");
        }

        // Get the player rank after submitting the score
        StartCoroutine(GetRankRoutine(0));
    }

    private IEnumerator GetRankRoutine(int level=0)
    {
        //Try and grab the Rank
        Debug.Log(m_rankURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(m_playerName), "&level=", level));
        WWW RankGrabAttempt = new WWW(m_rankURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(m_playerName)));

        yield return RankGrabAttempt;

        if (RankGrabAttempt.error == null)
        {
            Debug.LogFormat("Recieved Rank {0}", RankGrabAttempt.text);

            int rank;
            if(System.Int32.TryParse(RankGrabAttempt.text, out rank))
                EventManager.Fire(new RankRecievedEvent(rank));
        }
        else
        {
            Debug.Log("Failed to Get Rank");
        }
    }

    // Gets the top score for the player at the specified level. 
    // For simple games we can default this to just one level
    private IEnumerator GetTopScoresRoutine(int level=0)
    {
        yield return new WaitForSeconds(0.1f);
        Debug.Log(m_topScoresURL.ConcatString("database=", m_database, "&level=", level));
        WWW GetScoresAttempt = new WWW(m_topScoresURL.ConcatString("database=", m_database, "&level=", level));
        yield return GetScoresAttempt;

        if (GetScoresAttempt.error != null)
        {
            Debug.Log("Failed to get top scores");
        }
        else
        {
            Debug.LogFormat("Recieved Top Scores {0}", GetScoresAttempt.text);

            //Collect up all our data
            string[] textlist = GetScoresAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);

            //Split it into two smaller arrays
            string[] Names = new string[Mathf.FloorToInt(textlist.Length / 2)];
            string[] Scores = new string[Names.Length];
            for (int i = 0; i < textlist.Length; i++)
            {
                if (i % 2 == 0)
                    Names[Mathf.FloorToInt(i / 2)] = textlist[i];
                else
                    Scores[Mathf.FloorToInt(i / 2)] = textlist[i];
                //Debug.Log(textlist[i].ToString());
            }

            List<ScoreRecord> scoreRecords = new List<ScoreRecord>();
            for (int i = 0; i < Names.Length; i++)
            {
                scoreRecords.Add(new ScoreRecord(Names[i], Scores[i], (i + 1).ToString()));
            }

            EventManager.Fire(new TopScoreRecievedEvent(scoreRecords));
        }
    }

    // This routine is called when we need to fetch the aggregate top player scores for multiple levels.
    //private IEnumerator GetTopTotalScoresRoutine()
    //{
    //    yield return new WaitForSeconds(0.3f);
    //    Debug.Log(m_totalTopScoresURL.ConcatString("database=", m_database));
    //    WWW GetScoresAttempt = new WWW(m_totalTopScoresURL.ConcatString("database=", m_database));
    //    yield return GetScoresAttempt;

    //    if (GetScoresAttempt.error != null)
    //    {
    //        Debug.Log("Error in GetTopTotalScores");
    //    }
    //    else
    //    {
    //        //Collect up all our data
    //        string[] textlist = GetScoresAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);

    //        //Split it into two smaller arrays
    //        string[] Names = new string[Mathf.FloorToInt(textlist.Length / 2)];
    //        string[] Scores = new string[Names.Length];
    //        for (int i = 0; i < textlist.Length; i++)
    //        {
    //            if (i % 2 == 0)
    //            {
    //                Names[Mathf.FloorToInt(i / 2)] = textlist[i];
    //            }
    //            else Scores[Mathf.FloorToInt(i / 2)] = textlist[i];
    //        }

    //        List<TotalScoreRecord> scoreRecords = new List<TotalScoreRecord>();
    //        for (int i = 0; i < Names.Length; i++)
    //        {
    //            scoreRecords.Add(new TotalScoreRecord(Names[i], Scores[i], (i + 1).ToString()));
    //        }

    //        EventManager.Fire(new TopTotalScoresRecievedEvent(scoreRecords));
    //    }
    //}

    //private IEnumerator GetLevelScoresRoutineRoutine()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    Debug.Log(m_levelsCompleted.ConcatString("database=", m_database, "&name=", m_playerName));
    //    WWW GetLevelsAttempt = new WWW(m_levelsCompleted.ConcatString("database=", m_database, "&name=", m_playerName));
    //    yield return GetLevelsAttempt;

    //    if (GetLevelsAttempt.error != null)
    //    {
    //        Debug.Log("Failed to get levels passed");
    //    }
    //    else
    //    {
    //        Debug.LogFormat("Recieved Levels Passed {0}", GetLevelsAttempt.text);


    //        //Collect up all our data
    //        string[] textlist = GetLevelsAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);
    //        //Split it into two smaller arrays
    //        int[] Levels = new int[Mathf.FloorToInt(textlist.Length / 2)];
    //        int[] Scores = new int[Levels.Length];
    //        for (int i = 0; i < textlist.Length; i++)
    //        {
    //            if (i % 2 == 0)
    //                Levels[Mathf.FloorToInt(i / 2)] = System.Int32.Parse(textlist[i]);
    //            else
    //                Scores[Mathf.FloorToInt(i / 2)] = System.Int32.Parse(textlist[i]);
    //        }

    //        int totalScore = 0;
    //        for (int i = 0; i < Levels.Length; i++)
    //        {
    //            LevelScores[Levels[i]] = Scores[i];
    //            totalScore += Scores[i];
    //        }

    //        EventManager.Fire(new GetLevelsScoresEvent(LevelScores));
    //    }
    //}

    //private IEnumerator GrabTotalRankRoutine()
    //{
    //    Debug.LogFormat("Gettting rank for player {0}", m_playerName);
    //    //Try and grab the Rank
    //    WWW RankGrabAttempt = new WWW(m_totalRankURL.ConcatString("database=", m_database, "&name=", WWW.EscapeURL(m_playerName)));
    //    yield return RankGrabAttempt;

    //    if (RankGrabAttempt.error == null)
    //    {
    //        //Debug.LogFormat("Recieved Rank {0}", RankGrabAttempt.text);
    //        int rank;
    //        if (System.Int32.TryParse(RankGrabAttempt.text, out rank))
    //            EventManager.Fire(new TotalRankRecievedEvent(rank));
    //    }
    //    else
    //    {
    //        Debug.Log("Failed to Get Rank");
    //    }
    //}

    // For use for games with multiple levels where score records are kept per level.
    //private IEnumerator AggregateAllScoresRoutine()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    Debug.Log(m_levelsCompleted.ConcatString("database=", m_database, "&name=", m_playerName));
    //    WWW GetScoresAttempt = new WWW(m_levelsCompleted.ConcatString("database=", m_database, "&name=", m_playerName));
    //    yield return GetScoresAttempt;

    //    if (GetScoresAttempt.error != null)
    //    {
    //        Debug.Log("Failed to get levels passed");
    //    }
    //    else
    //    {
    //        Debug.LogFormat("Recieved Levels Passed {0}", GetScoresAttempt.text);

    //        //Collect up all our data
    //        string[] textlist = GetScoresAttempt.text.Split(new string[] { "\n", "\t" }, System.StringSplitOptions.RemoveEmptyEntries);
    //        //Split it into two smaller arrays
    //        int[] Levels = new int[Mathf.FloorToInt(textlist.Length / 2)];
    //        int[] Scores = new int[Levels.Length];
    //        for (int i = 0; i < textlist.Length; i++)
    //        {
    //            if (i % 2 == 0)
    //                Levels[Mathf.FloorToInt(i / 2)] = System.Int32.Parse(textlist[i]);
    //            else
    //                Scores[Mathf.FloorToInt(i / 2)] = System.Int32.Parse(textlist[i]);
    //        }

    //        int totalScore = 0;
    //        for (int i = 0; i < Levels.Length; i++)
    //        {
    //            LevelScores[Levels[i]] = Scores[i];
    //            totalScore += Scores[i];
    //        }

    //        EventManager.Fire(new TotalScoreRecievedEvent(totalScore));
    //    }
    //}
    #endregion // Coroutines

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable()
    {
        EventManager.AddListener<PlayerNameChangedEvent>(OnPlayerNameChangedEvent);
        EventManager.AddListener<ScoreChangedEvent>(OnScoreChangedEvent);
        EventManager.AddListener<RankRecievedEvent>(OnRankRecievedEvent);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<PlayerNameChangedEvent>(OnPlayerNameChangedEvent);
        EventManager.RemoveListener<ScoreChangedEvent>(OnScoreChangedEvent);
        EventManager.RemoveListener<RankRecievedEvent>(OnRankRecievedEvent);
    }

    private void OnPlayerNameChangedEvent(PlayerNameChangedEvent evt)
    {
        m_playerName = evt.PlayerName;
    }

    private void OnScoreChangedEvent(ScoreChangedEvent evt)
    {
        m_playerScore = evt.Score;
        StartCoroutine(AddScoreRoutine(score:m_playerScore));
    }

    private void OnRankRecievedEvent(RankRecievedEvent evt)
    {
        m_playerRank = evt.Rank;
    }
}
