using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreBoardMenuController : MonoBehaviour
{
    private const string MainMenu = "Menu";

    private void Update()
    {
        if (Input.GetButtonDown("Fire3")) // B
        {
            OnClickBack();
        }
    }

    public void OnClickBack()
    {
        SceneManager.LoadScene(MainMenu);
    }
    public void OnClickTest()
    {
        LeaderBoards leader = FindObjectOfType<LeaderBoards>();
        leader.TestAddScore();
    }
}
