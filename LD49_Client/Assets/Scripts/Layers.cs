using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Layers
{
    public static int Player => m_player.Value;
    private static Lazy<int> m_player = new Lazy<int>(() => LayerMask.NameToLayer("Player"));
    public static int Chef => m_chef.Value;
    private static Lazy<int> m_chef = new Lazy<int>(() => LayerMask.NameToLayer("Chef"));
    public static int Ingredient => m_ingredient.Value;
    private static Lazy<int> m_ingredient = new Lazy<int>(() => LayerMask.NameToLayer("Ingredient"));
    public static int FoodStack => m_foodStack.Value;
    private static Lazy<int> m_foodStack = new Lazy<int>(() => LayerMask.NameToLayer("FoodStack"));
    public static int Plate => m_plate.Value;
    private static Lazy<int> m_plate = new Lazy<int>(() => LayerMask.NameToLayer("Plate"));
    public static int Floor => m_floor.Value;
    private static Lazy<int> m_floor = new Lazy<int>(() => LayerMask.NameToLayer("Floor"));
    public static int Garbage => m_garbage.Value;
    private static Lazy<int> m_garbage = new Lazy<int>(() => LayerMask.NameToLayer("Garbage"));

    public enum Layer
    {

    }
}
