using System.Collections;
using UnityEngine;

public class IngredientController : MonoBehaviour
{
    [SerializeField] public IngredientType IngredientType;

    const float LifeSpan = 10f;

    private void Start()
    {
        StartCoroutine(ChangeFloorLayerRoutine());
    }

    private IEnumerator ChangeFloorLayerRoutine()
    {
        yield return new WaitForSeconds(LifeSpan);
        gameObject.layer = LayerMask.NameToLayer("DirtyFood");
    }
}
