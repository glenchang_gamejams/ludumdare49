using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBubble : MonoBehaviour
{
    [SerializeField]
    private GameObject m_happyBubble;

    [SerializeField]
    private GameObject m_angryBubble;

    [SerializeField]
    private GameObject m_burgerBubble;

    private void OnEnable()
    {
        m_happyBubble.SetActive(false);
        m_angryBubble.SetActive(false);
        m_burgerBubble.SetActive(false);
    }

    public void Leave(bool isHappy)
    {
        m_happyBubble.SetActive(isHappy);
        m_angryBubble.SetActive(!isHappy);
        m_burgerBubble.SetActive(false);
    }

    public void NewOrder()
    {
        m_happyBubble.SetActive(false);
        m_angryBubble.SetActive(false);
        m_burgerBubble.SetActive(true);
    }    
}
