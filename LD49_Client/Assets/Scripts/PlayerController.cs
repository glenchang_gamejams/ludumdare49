using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    const float HorizontalSpeed = .1f;
    const float VerticalSpeed = .1f;

    public DropOffZone dropZone = DropOffZone.None;

    [SerializeField] Camera m_camera;
    [SerializeField] Transform m_headBone;
    [SerializeField] Vector3 m_headTurnLimit = new Vector3(40f, 120f, 0f);

    bool UseRightStick = false;

    Vector3 m_targetLookDir = Vector3.zero;
    private Transform m_lookAt = null;

    public enum DropOffZone
    {
        None,
        Left,
        Right
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DoTranslation();
        DoRotation();
    }

    // For head look at to happen after animation
    private void LateUpdate()
    {
        if (m_lookAt == null)
            return;

        var lookRotation = Quaternion.LookRotation(m_lookAt.position - m_headBone.position );
        m_headBone.rotation = Quaternion.RotateTowards(m_headBone.rotation, lookRotation, 1f);
        //m_headBone.localEulerAngles = new Vector3(
        //    m_headBone.localEulerAngles.x,
        //    Mathf.Min(m_headTurnLimit.y, m_headBone.localEulerAngles.y),
        //    0f
        //    );
        //m_headBone.localEulerAngles = new Vector3(
        //    Mathf.Min(m_headTurnLimit.x, Mathf.Max(-m_headTurnLimit.x, m_headBone.localEulerAngles.x)),
        //    Mathf.Min(m_headTurnLimit.y, Mathf.Max(-m_headTurnLimit.y, m_headBone.localEulerAngles.y)),
        //    m_headBone.localEulerAngles.z
        //    );
    }

    private void DoTranslation()
    {
        float dx = Input.GetAxis("Horizontal") * HorizontalSpeed;
        float dz = Input.GetAxis("Vertical") * VerticalSpeed;
        transform.position = new Vector3(transform.position.x + dx, transform.position.y, transform.position.z + dz);
    }

    private void DoRotation()
    {
        var dx = Input.GetAxis("Horizontal2");
        var dz = Input.GetAxis("Vertical2");
        // Enable right stick controls if they move the stick far enough
        if (dx*dx > .2f || dz*dz > .2f)
            UseRightStick = true;

        var lookPos = Vector3.zero;

        if(UseRightStick)
        {
            if (dx * dx < .1f && dz * dz < .1f)
                m_targetLookDir = transform.forward;
            else
                m_targetLookDir = new Vector3(dx, 0, dz).normalized;
        }
        else
        {
            var cameraPos = m_camera.transform.position;
            var nearLookPos = m_camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
            var dh = cameraPos.y - nearLookPos.y;
            var factor = (cameraPos.y - transform.position.y) / dh;
            m_targetLookDir = (new Vector3(cameraPos.x + (nearLookPos.x - cameraPos.x) * factor, transform.position.y, cameraPos.z + (nearLookPos.z - cameraPos.z) * factor) - transform.position).normalized;
        }

        var currentLookAt = transform.position + transform.forward;
        var targetLookAt = transform.position + m_targetLookDir;
        transform.LookAt(currentLookAt + (targetLookAt - currentLookAt)*.1f);
    }


    private void OnEnable()
    {
        EventManager.AddListener<LaunchedIngredientEvent>(OnLaunchedIngredientEvent);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<LaunchedIngredientEvent>(OnLaunchedIngredientEvent);
    }

    private void OnLaunchedIngredientEvent(LaunchedIngredientEvent evt)
    {
        m_lookAt = evt.Ingredient;
    }
}
