using System;
using System.Collections.Generic;
using UnityEngine;

public class RecipePool : MonoBehaviour
{
    [SerializeField] List<Recipe> Recipes;

    [SerializeField] RecipeTier[] RecipesTiers;

    [SerializeField] int NumBurgersToNextTier = 2;

    public int OrderCount = 0;
    public int OrderTier = 0;
    private int elapsedCountToNextTier = 0;

    [Serializable]
    public class RecipeTier
    {
        public List<Recipe> Recipes;
    }

    public Recipe GetNext()
    {
        OrderCount++;
        elapsedCountToNextTier++;
        if (elapsedCountToNextTier == NumBurgersToNextTier)
        {
            IncreaseTier();
            elapsedCountToNextTier = 0;
        }

        var randTier = UnityEngine.Random.Range(0, OrderTier);
        var index = UnityEngine.Random.Range(0, RecipesTiers[randTier].Recipes.Count);
        return RecipesTiers[randTier].Recipes[index];
    }

    public void IncreaseTier()
    {
        OrderTier = Mathf.Min(RecipesTiers.Length, OrderTier + 1);
        Debug.Log($"Order Tier increased to {OrderTier}");
    }
}
