using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    private const string PlaySceneName = "Main";
    private const string OptionsSceneName = "Options";
    private const string ScoreBoardSceneName = "ScoreBoard";
    private const string TutorialSceneName = "Tutorial";

    private const string PlayerNamePlayerPrefs = "PlayerName";

    [SerializeField]
    private InputField m_playerNameField;

    [SerializeField] TextBounce Play;
    [SerializeField] TextBounce Scoreboard;
    [SerializeField] TextBounce Tutorial;
    [SerializeField] TextBounce Options;
    bool SelectionChanged = false;
    int SelectedIndex = 0;
    
    [SerializeField]
    private GameObject m_playerNamePanel;

    public void Start()
    {
        m_playerNamePanel.SetActive(false);
    }

    string GenerateChefName()
    {
        var Titles = new string[] {
            "Chef",
            "Culinary",
            "Cook",
            "Gourmet",
            "Fryer",
            "Waiter",
            "Server"
        };
        var Names = new string[] {
            "Craig",
            "Ken",
            "Carl",
            "Gary",
            "Fred",
            "Walt",
            "Sam"
        };

        var index = Random.Range(0, Titles.Length);
        var title = Titles[index];
        index = Random.Range(0, Names.Length);
        var name = Names[index];
        var unique = System.DateTime.UtcNow.Ticks;
        var bytes = System.BitConverter.GetBytes(unique);
        var hex = System.BitConverter.ToString(bytes).Replace("-", "").Substring(0, 5);
        return $"{title} {name}#{hex}";
    }

    public void OnSetPlayerName()
    {
        // If no name do nothing
        if (string.IsNullOrEmpty(m_playerNameField.text))
            return;

        EventManager.Fire(new PlayerNameChangedEvent(m_playerNameField.text));
        PlayerPrefs.SetString(PlayerNamePlayerPrefs, m_playerNameField.text);
        SceneManager.LoadScene(PlaySceneName);
    }
    
    public void OnClickPlay()
    {
        m_playerNamePanel.SetActive(true);
        m_playerNameField.text = PlayerPrefs.HasKey(PlayerNamePlayerPrefs) ? PlayerPrefs.GetString(PlayerNamePlayerPrefs) : GenerateChefName();
        m_playerNameField.Select();
    }

    public void OnClickOptions()
    {
        SceneManager.LoadScene(OptionsSceneName);
    }

    public void OnClickScoreBoard()
    {
        SceneManager.LoadScene(ScoreBoardSceneName);
    }

    public void OnClickTutorial()
    {
        SceneManager.LoadScene(TutorialSceneName);
    }

    private void Update()
    {
        if (m_playerNamePanel.activeInHierarchy)
        {
            if (Input.GetButtonDown("Fire2")) // A
            {
                OnSetPlayerName();
            }
        }
        else
        {
            HandleMenuSelections();
        }
    }

    void HandleMenuSelections()
    {
        // handle resetting the axis state so we don't just jump straight to the first or last options
        if (SelectionChanged)
        {
            SelectionChanged = Mathf.Abs(Input.GetAxis("Vertical")) > 0.5f || Mathf.Abs(Input.GetAxis("DPadVertical")) > 0.5f;
            return;
        }

        if (Input.GetAxis("Vertical") < -0.5f || Input.GetAxis("DPadVertical") < -0.5f)
        {
            SelectionChanged = true;
            SelectedIndex++;
        }
        if (Input.GetAxis("Vertical") > 0.5 || Input.GetAxis("DPadVertical") > 0.5f)
        {
            SelectionChanged = true;
            SelectedIndex--;
        }
        SelectedIndex = Mathf.Clamp(SelectedIndex, 0, 3);

        switch (SelectedIndex)
        {
            case 0: // Play
                Play.enabled = true;

                Scoreboard.enabled = false;
                Scoreboard.transform.localScale = Vector2.one;

                Tutorial.enabled = false;
                Tutorial.transform.localScale = Vector2.one;

                Options.enabled = false;
                Options.transform.localScale = Vector2.one;
                break;

            case 1: // Score
                Play.enabled = false;
                Play.transform.localScale = Vector2.one;

                Scoreboard.enabled = true;

                Tutorial.enabled = false;
                Tutorial.transform.localScale = Vector2.one;

                Options.enabled = false;
                Options.transform.localScale = Vector2.one;
                break;

            case 2: // Tutorial
                Play.enabled = false;
                Play.transform.localScale = Vector2.one;

                Scoreboard.enabled = false;
                Scoreboard.transform.localScale = Vector2.one;

                Tutorial.enabled = true;

                Options.enabled = false;
                Options.transform.localScale = Vector2.one;
                break;

            case 3: // Options
                Play.enabled = false;
                Play.transform.localScale = Vector2.one;

                Scoreboard.enabled = false;
                Scoreboard.transform.localScale = Vector2.one;

                Tutorial.enabled = false;
                Tutorial.transform.localScale = Vector2.one;

                Options.enabled = true;
                break;
        }

        if (Input.GetButtonDown("Fire2")) // A
        {
            if (SelectedIndex == 0) OnClickPlay();
            if (SelectedIndex == 1) OnClickScoreBoard();
            if (SelectedIndex == 2) OnClickTutorial();
            if (SelectedIndex == 3) OnClickOptions();
        }
    }
}
