using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChefController : MonoBehaviour
{
    [SerializeField, Range(1, 10)] public float ThrowHeight;
    [SerializeField] float LaunchBuffer;
    [SerializeField] FoodStackController FoodStack;
    [SerializeField] GameObject Beef;
    [SerializeField] GameObject Cheese;
    [SerializeField] GameObject Lettuce;
    [SerializeField] GameObject Tomato;
    [SerializeField] GameObject Bun;

    [SerializeField] Transform SpawnPoint;
    [SerializeField] Renderer IngredientLandingZone;
    [SerializeField] Vector3 LaunchTorque;
    [SerializeField] SFXPool YellSFX;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump")) // Y
        {
            LaunchCheese();
        }

        if (Input.GetButtonDown("Fire1")) // X
        {
            LaunchBeef();
        }

        if (Input.GetButtonDown("Fire2")) // A
        {
            LaunchLettuce();
        }

        if (Input.GetButtonDown("Fire3")) // B
        {
            LaunchTomato();
        }

        if (Input.GetButtonDown("Bun")) // Rb
        {
            LaunchBun();
        }
    }

    public void LaunchBeef()
    {
        LaunchIngredient(Beef);
    }
    public void LaunchCheese()
    {
        LaunchIngredient(Cheese);
    }
    public void LaunchLettuce()
    {
        LaunchIngredient(Lettuce);
    }
    public void LaunchTomato()
    {
        LaunchIngredient(Tomato);
    }
    public void LaunchBun()
    {
        LaunchIngredient(Bun);
    }
    private void LaunchIngredient(GameObject ingredient)
    {
        //if (!BuyIngredient()) return;

        // Distance formula
        // p(t) = p0 + vt + .5*at^2
        // Take derivative for velocity formula
        // dp/dt = v + at
        // Determine time at peak (dp/dt (velocity) = 0)
        // 0 = v + at
        // t = -v/a
        // Replace to create a function for height at peak based on initial velocity
        // h(v) = p0 - v^2/(a) + .5*av^2/(a^2)
        // h(v) = p0 - av^2/(a^2) + .5*av^2/(a^2)
        // h(v) = p0 - .5*av^2/(a^2)
        // h(v) = p0 - .5*v^2/a
        // Solve for v (initial velocity to reach height at peak)
        // v(h) = (2a(p0-h))^.5
        // Determine time when arc reaches zero (quadratic formula)
        // t = (-v + (v^2 - 4*p0*.5*a)^.5)/(a)

        // initial position
        var p0 = SpawnPoint.position;
        // acceleration
        var a = Physics.gravity.y;
        // height of arc's peak
        var h = FoodStack.Height + ThrowHeight;
        // initial velocity to reach arc's peak
        var v = Mathf.Sqrt(2 * a * (p0.y - h));
        // time when position is 0
        var t = (-v - Mathf.Sqrt(v * v - (4 * p0.y * .5f * a))) / (a);
        var t2 = (-v + Mathf.Sqrt(v * v - (4 * p0.y * .5f * a))) / (a);

        var gameObject = Instantiate(ingredient);
        gameObject.transform.position = p0;
        var rigidBody = gameObject.GetComponent<Rigidbody>();
        bool foundTarget = false;
        float targetX = 0;
        float targetZ = 0;
        var bounds = IngredientLandingZone.bounds;
        while(!foundTarget)
        {
            targetX = Random.Range(-bounds.extents.x, bounds.extents.x) + bounds.center.x;
            targetZ = Random.Range(-bounds.extents.z, bounds.extents.z) + bounds.center.z;
            // Make sure target isn't too close to the chef
            foundTarget = ((targetX - transform.position.x) * (targetX - transform.position.x) + (targetZ - transform.position.z) * (targetZ - transform.position.z)) > LaunchBuffer;
        }
        var dx = targetX - SpawnPoint.position.x;
        var dz = targetZ - SpawnPoint.position.z;
        //var tmp = Instantiate(ingredient);
        //tmp.transform.position = new Vector3(SpawnPoint.position.x + dx, 0, SpawnPoint.position.z + dz);
        //tmp.GetComponent<MeshCollider>().enabled = false;
        //tmp.GetComponent<Rigidbody>().useGravity = false;
        //var tmp2 = Instantiate(ingredient);
        //tmp2.transform.position = new Vector3(SpawnPoint.position.x + dx / 2, h, SpawnPoint.position.z + dz / 2);
        //tmp2.GetComponent<MeshCollider>().enabled = false;
        //tmp2.GetComponent<Rigidbody>().useGravity = false;
        //StartCoroutine(DestroyLater(tmp, 2));
        //StartCoroutine(DestroyLater(tmp2, 2));
        //Debug.Log($"p0: {p0.y}, h: {h}, v: {v}, t: {t}({t2})");

        var x = dx / t;
        var z = dz / t;
        rigidBody.AddForce(new Vector3(x, v, z), ForceMode.VelocityChange);
        rigidBody.AddTorque(LaunchTorque);
        YellSFX.Play();

        EventManager.Fire(new LaunchedIngredientEvent(rigidBody.transform));
    }

    private bool BuyIngredient()
    {
        // TODO how much money should this actually cost??
        float cost = 1;
        return GameController.SpendMoney(cost);
    }

    IEnumerator DestroyLater(GameObject go, int seconds)
    {
        yield return new WaitForSeconds(seconds);
        DestroyImmediate(go);
    }

}
