public enum IngredientType
{
    Burger,
    Cheese,
    Lettuce,
    Tomato,
    Bun
}
