using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{
    [SerializeField]
    private float m_completeWaitTime = 1.2f;

    [SerializeField]
    private NavMeshAgent[] m_npcPrefabList;

    [SerializeField]
    private List<Transform> m_waypoints = new List<Transform>();

    [SerializeField]
    private ChefController m_chef;

    [SerializeField]
    private List<Transform> m_chefWaypoints = new List<Transform>();

    [SerializeField]
    private Vector2 m_chefMoveTimeRange = new Vector2(1f, 3f);

    private float chefElapsedTime = 0f;
    private float nextChefElapsedTime = 0f;

    [SerializeField]
    private Renderer m_spawnArea;

    [SerializeField]
    private Renderer m_dropZoneRight;

    [SerializeField]
    private Renderer m_dropZoneLeft;

    [SerializeField]
    private GameObject m_markers;

    private Queue<NavMeshAgent> m_agents = new Queue<NavMeshAgent>();
    private Queue<Transform> m_occupiedWaypoints = new Queue<Transform>();
    private PlayerController m_player;

    public NavMeshAgent CurrentAgent => m_agents.Peek();

    public PlayerController.DropOffZone OrderZone = PlayerController.DropOffZone.None;

    public void CustomerAdd()
    {
        // If waypoints full, don't add
        if (m_occupiedWaypoints.Count == m_waypoints.Count)
            return;

        // Instantiate an agent
        NavMeshAgent npcAgent = Instantiate<NavMeshAgent>(m_npcPrefabList[Random.Range(0, m_npcPrefabList.Length)]);
        m_agents.Enqueue(npcAgent);

        StartCoroutine(CustomerAddRoutine(npcAgent));
    }

    private IEnumerator CustomerAddRoutine(NavMeshAgent npcAgent)
    {
        // Randomize an unoccupied waypoint
        var unoccupiedWaypoints = m_waypoints.Except(m_occupiedWaypoints);
        int randIndex = Random.Range(0, unoccupiedWaypoints.Count());
        var waypoint = unoccupiedWaypoints.ElementAt(randIndex);
        m_occupiedWaypoints.Enqueue(waypoint);

        // Position in spawn area
        Bounds spawnBounds = m_spawnArea.bounds;
        Vector3 spawnLocation = spawnBounds.center + new Vector3(Random.Range(-spawnBounds.extents.x, spawnBounds.extents.x), 0f, 0f);
        npcAgent.transform.position = spawnLocation;
        npcAgent.SetDestination(waypoint.position);

        Animator animator = npcAgent.GetComponentInChildren<Animator>();
        animator.SetFloat("WalkSpeed", 1f);

        yield return new WaitForSeconds(2f);
        animator.SetFloat("WalkSpeed", 0f);

        // If first customer turn on burger bubble
        if (m_agents.Count == 1)
        {
            NPCBubble bubble = npcAgent.GetComponent<NPCBubble>();
            bubble.NewOrder();

            bool isLeft = npcAgent.transform.position.x < 0 ? true : false;
            OrderZone = isLeft ? PlayerController.DropOffZone.Left : PlayerController.DropOffZone.Right;
            m_dropZoneLeft.gameObject.SetActive(isLeft);
            m_dropZoneRight.gameObject.SetActive(!isLeft);
        }
    }

    public void CustomerRemove()
    {
        if (m_agents.Count == 0)
            return;

        StartCoroutine(CustomerRemoveRoutine());
    }

    private IEnumerator CustomerRemoveRoutine()
    {
        m_dropZoneLeft.gameObject.SetActive(false);
        m_dropZoneRight.gameObject.SetActive(false);

        // Wait for text bubble
        yield return new WaitForSeconds(1f);

        // Leave
        m_occupiedWaypoints.Dequeue();
        var npcAgent = m_agents.Dequeue();
        Bounds spawnBounds = m_spawnArea.bounds;
        Vector3 destination = spawnBounds.center + new Vector3(Random.Range(-spawnBounds.extents.x, spawnBounds.extents.x), 0f, 0f);
        npcAgent.SetDestination(destination);
        npcAgent.isStopped = false;
        Destroy(npcAgent.gameObject, 3f);

        Animator animator = npcAgent.GetComponentInChildren<Animator>();
        animator.SetFloat("WalkSpeed", 1f);

        // Start new order bubble
        if (m_agents.Count >= 1)
        {
            var nextAgent = m_agents.Peek();
            NPCBubble bubble = nextAgent.GetComponent<NPCBubble>();
            bubble.NewOrder();

            bool isLeft = nextAgent.transform.position.x < 0 ? true : false;
            OrderZone = isLeft ? PlayerController.DropOffZone.Left : PlayerController.DropOffZone.Right;
            m_dropZoneLeft.gameObject.SetActive(isLeft);
            m_dropZoneRight.gameObject.SetActive(!isLeft);
        }

        float elapsedTime = 0f;
        while(elapsedTime < 2f)
        {
            elapsedTime += Time.deltaTime;

            // If agent arrived stop
            if (npcAgent.remainingDistance <= npcAgent.stoppingDistance && !npcAgent.hasPath || npcAgent.velocity.sqrMagnitude == 0f)
            {
                animator.SetFloat("WalkSpeed", 0f);
                break;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private void Start()
    {
        if (m_player == null)
            m_player = FindObjectOfType<PlayerController>();

        if (m_chef == null)
            m_chef = FindObjectOfType<ChefController>();

        nextChefElapsedTime = Random.Range(m_chefMoveTimeRange.x, m_chefMoveTimeRange.y);
    }

    private void OnEnable()
    {
        // Hide our renderers
        m_markers.HideRenderers();
        m_dropZoneLeft.gameObject.SetActive(false);
        m_dropZoneRight.gameObject.SetActive(false);

        EventManager.AddListener<CustomerEvent>(OnCustomerEvent);
        EventManager.AddListener<NewOrderEvent>(OnNewOrderEvent);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<CustomerEvent>(OnCustomerEvent);
        EventManager.RemoveListener<NewOrderEvent>(OnNewOrderEvent);
    }

    public void OnCustomerEvent(CustomerEvent evt)
    {
        // Change bubble
        if (m_agents.Count >= 1)
        {
            var currentAgent = m_agents.Peek();
            NPCBubble bubble = currentAgent.GetComponent<NPCBubble>();
            bubble.Leave(evt.IsHappy);
        }

        CustomerRemove();
    }

    public void OnNewOrderEvent(NewOrderEvent evt)
    {
        CustomerAdd();
    }

    IEnumerator MoveChefRoutine()
    {
        var waypoint = m_chefWaypoints[Random.Range(0, m_chefWaypoints.Count)];
        float elapsedTime = 0f;
        while(elapsedTime < 1f)
        {
            elapsedTime += Time.deltaTime;
            m_chef.transform.position = Vector3.Lerp(m_chef.transform.position, waypoint.transform.position, Time.deltaTime / 1f);
            yield return new WaitForEndOfFrame();
        }
    }

    private void Update()
    {
        // Make ordering agents look at player
        foreach(var agent in m_agents)
        {
            // Skip if agent is done moving
            if (agent.remainingDistance <= agent.stoppingDistance && !agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                agent.transform.rotation = Quaternion.RotateTowards(agent.transform.rotation, Quaternion.LookRotation(m_player.transform.position - agent.transform.position), 0.5f);
        }

        chefElapsedTime += Time.deltaTime;
        if (chefElapsedTime > nextChefElapsedTime)
        {
            nextChefElapsedTime = Random.Range(m_chefMoveTimeRange.x, m_chefMoveTimeRange.y);
            chefElapsedTime = 0f;
            StartCoroutine(MoveChefRoutine());
        }
    }
}
