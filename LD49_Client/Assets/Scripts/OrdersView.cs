using System.Collections.Generic;
using UnityEngine;

public class OrdersView : MonoBehaviour
{
    [SerializeField] RecipeView RecipeViewSource;
    Dictionary<int, RecipeView> OrderViews = new Dictionary<int, RecipeView>();

    public void AddOrder(int index, Recipe recipe)
    {
        var view = Instantiate<RecipeView>(RecipeViewSource, transform);
        view.PopulateFor(recipe);
        OrderViews[index] = view;
    }

    public void RemoveOrder(int index)
    {
        var view = OrderViews[index];
        Destroy(view.gameObject);
        OrderViews.Remove(index);
    }

    public void SetCurrent(int index)
    {
        foreach (var pair in OrderViews)
            pair.Value.SetCurrent(pair.Key == index);
    }
}
