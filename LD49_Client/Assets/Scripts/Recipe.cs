using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Recipe : ScriptableObject
{
    [SerializeField] public string Name;
    [SerializeField] public List<IngredientType> Ingedients;
    [SerializeField] public float TimeEarned;
    [SerializeField] public float MoneyEarned;
}
