using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPopupController : MonoBehaviour
{
    const string TryAgainSceneName = "Main";
    const string QuitSceneName = "Menu";
    private const string ScoreBoardSceneName = "ScoreBoard";

    [SerializeField] Text HighScoreText;
    [SerializeField] TextBounce TryAgain;
    [SerializeField] TextBounce Quit;
    int SelectedIndex = 0;

    [SerializeField] Text RankText;

    public void OnClickTryAgain() => SceneManager.LoadScene(TryAgainSceneName);

    public void OnClickQuit() => SceneManager.LoadScene(QuitSceneName);

    public void SetHighScore(float moneyEarned)
    {
        HighScoreText.text = $"You earned {moneyEarned} today!";
    }

    private void Update()
    {
        if (Input.GetAxis("Vertical") < -0.5f || Input.GetAxis("DPadVertical") < -0.5f) SelectedIndex++;
        if (Input.GetAxis("Vertical") > 0.5 || Input.GetAxis("DPadVertical") > 0.5f) SelectedIndex--;
        SelectedIndex = Mathf.Clamp(SelectedIndex, 0, 1);

        TryAgain.enabled = SelectedIndex == 0;
        if (SelectedIndex == 1)
            TryAgain.transform.localScale = Vector2.one;

        Quit.enabled = SelectedIndex == 1;
        if (SelectedIndex == 0)
            Quit.transform.localScale = Vector2.one;

        if (Input.GetButtonDown("Fire2")) // A
        {
            if (SelectedIndex == 0) OnClickTryAgain();
            if (SelectedIndex == 1) OnClickQuit();
        }
    }
    public void OnClickScoreBoard()
    {
        SceneManager.LoadScene(ScoreBoardSceneName);
    }
    
    private void OnEnable()
    {
        EventManager.AddListener<RankRecievedEvent>(OnRankRecievedEvent);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<RankRecievedEvent>(OnRankRecievedEvent);
    }

    private void OnRankRecievedEvent(RankRecievedEvent evt)
    {
        RankText.text = evt.Rank.ToString();
    }
}
