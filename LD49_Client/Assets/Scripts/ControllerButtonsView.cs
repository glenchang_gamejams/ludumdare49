using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerButtonsView : MonoBehaviour
{
    [SerializeField] List<Image> Buttons;

    public void SetVisible(bool visible)
    {
        foreach (var b in Buttons)
            b.gameObject.SetActive(visible);
    }
}
