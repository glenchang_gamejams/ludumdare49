using System.Collections;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    [SerializeField] AudioSource Happy;
    [SerializeField] AudioSource Angry;
    [SerializeField] AudioSource Kaching;
    [SerializeField] AudioSource VictorySting;

    void HandleCustomerEvent(CustomerEvent customerEvent)
    {
        if (customerEvent.IsHappy)
            StartCoroutine(HappyRoutine());
        else
            Angry.Play();
    }

    private IEnumerator HappyRoutine()
    {
        VictorySting.Play();
        yield return new WaitForSeconds(0.2f);
        Kaching.Play();
        yield return new WaitForSeconds(1f);
        Happy.Play();
    }

    private void OnEnable()
    {
        EventManager.AddListener<CustomerEvent>(HandleCustomerEvent);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener<CustomerEvent>(HandleCustomerEvent);
    }
}
