using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    float StartingMoney = 25;
    [SerializeField]
    float SecondsRemaining = 120;

    const string PSController = "Sony Interactive Entertainment Wireless Controller";
    const string XBoxController = "Controller (XBOX 360 For Windows)";

    public static GameController Instance { get; set; }
    public static bool SpendMoney(float money)
    {
        if (Instance == null) return false;
        if (Instance.Money < money) return false;

        Instance.SetMoney(Instance.Money - money);
        return true;
    }

    [SerializeField] RecipePool RecipePool;
    int UniqueOrderIndex = 0;
    Dictionary<int, Recipe> CurrentOrders = new Dictionary<int, Recipe>();
    int CurrentOrderIndex = -1;
    int MaxOrders = 3;

    float OrderCountdown = 2;
    float MaxOrderCountdown = 5;

    [SerializeField] ChefController Chef;

    public PlayerController Player = null;
    float Money;
    float SecondsInBusiness = 0;

    [SerializeField] HUD HUD;
    [SerializeField] OrdersView OrdersView;
    [SerializeField] GameOverPopupController GameOverPopup;

    [SerializeField] ControllerButtonsView XboxButtons;
    [SerializeField] ControllerButtonsView PSButtons;
    [SerializeField] ControllerButtonsView PCButtons;

    [SerializeField] NPCController NPC;
    [SerializeField] ParticleSystem ChachingParticles;
    [SerializeField] FoodStackController FoodStack;
    [SerializeField] Transform IngredientLandingZoneScale;

    private void Start()
    {
        Instance = this;
        SetMoney(StartingMoney);
        StartCoroutine(UpdateRoutine());
        ChachingParticles.gameObject.SetActive(false);

        // handle showing a controller
        var joysticks = Input.GetJoystickNames();
        if (joysticks?.Length > 0)
        {
            var joystick = joysticks[0];
            PSButtons.SetVisible(joystick == PSController);
            XboxButtons.SetVisible(joystick != PSController); // assume xbox controller otherwise
            PCButtons.SetVisible(false);
        }
        else
        {
            PSButtons.SetVisible(false);
            XboxButtons.SetVisible(false);
        }
    }

    private IEnumerator UpdateRoutine()
    {
        while (!IsGameOver())
        {
            HandleTimeRemaining();
            HandleAddingOrders();
            HandleCompletingOrders();
            yield return new WaitForEndOfFrame();
        }

        HandleGameOver();
    }

    void HandleTimeRemaining()
    {
        SecondsRemaining -= Time.deltaTime;
        HUD.SetTime(SecondsRemaining);
        SecondsInBusiness += Time.deltaTime;
    }

    void HandleAddingOrders()
    {
        OrderCountdown -= Time.deltaTime;
        if (OrderCountdown <= 0.0f)
        {
            OrderCountdown = MaxOrderCountdown;

            // Add a new recipe to the current Orders
            if (CurrentOrders.Count < MaxOrders)
            {
                var next = RecipePool.GetNext();

                UniqueOrderIndex++;

                CurrentOrders[UniqueOrderIndex] = next;
                OrdersView.AddOrder(UniqueOrderIndex, next);

                EventManager.Fire(new NewOrderEvent());

                if (CurrentOrderIndex == -1)
                    SetCurrentOrderIndex(CurrentOrders.Keys.Min());
            }
        }
    }

    void HandleCompletingOrders()
    {
        if (Player != null)
        {
            // Check that the player is dropping off in the right zone
            if (NPC.OrderZone != Player.dropZone)
                return;

            var recipe = GetCurrentRecipe();
            if (CompleteBurger(FoodStack.GetIngedients(), recipe))
            {
                // Determine if we're dropping it off on the right zone. 
                Player.dropZone = PlayerController.DropOffZone.None;

                FoodStack.ClearStack();

                CurrentOrders.Remove(CurrentOrderIndex);
                OrdersView.RemoveOrder(CurrentOrderIndex);

                if (CurrentOrders.Count > 0)
                    SetCurrentOrderIndex(CurrentOrders.Keys.Min());
                else
                    SetCurrentOrderIndex(-1);

                IncreaseDifficulty();
            }
        }
    }

    void IncreaseDifficulty()
    {
        IngredientLandingZoneScale.localScale += new Vector3(.2f, 0, .1f);
        Chef.ThrowHeight += .2f;
    }

    IEnumerator CompleteOrderFXRoutine()
    {
        ChachingParticles.gameObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        ChachingParticles.gameObject.SetActive(false);
    }

    void HandleGameOver()
    {
        int score = (int)Mathf.Max(0f, Money - StartingMoney);
        GameOverPopup.SetHighScore(score);
        GameOverPopup.gameObject.SetActive(true);

        EventManager.Fire(new ScoreChangedEvent(score));
    }

    bool IsGameOver() => SecondsRemaining <= 0;

    void SetCurrentOrderIndex(int newIndex)
    {
        CurrentOrderIndex = newIndex;
        OrdersView.SetCurrent(CurrentOrderIndex);
    }

    void SetMoney(float newMoney)
    {
        Money = newMoney;
        HUD.SetMoney(Money);
    }

    Recipe GetCurrentRecipe() => HasCurrentOrder() ? CurrentOrders[CurrentOrderIndex] : null;

    bool HasCurrentOrder() => CurrentOrders.Count > 0 && CurrentOrderIndex >= 0;

    bool CompleteBurger(List<IngredientType> ingredients, Recipe recipe)
    {
        if (ingredients.Count == 0) return false;
        if (ingredients.Last() != IngredientType.Bun) return false;
        if (recipe == null) return false;

        if (MatchesRecipe(ingredients, recipe))
        {
            SetMoney(Money + recipe.MoneyEarned);
            SecondsRemaining += recipe.TimeEarned;
            EventManager.Fire(new CustomerEvent(true));

            // Do FX
            StartCoroutine(CompleteOrderFXRoutine());
        }
        else
        {
            EventManager.Fire(new CustomerEvent(false));
        }

        return true;
    }

    bool MatchesRecipe(List<IngredientType> ingredients, Recipe recipe)
    {
        var recipeIngedients = new List<IngredientType>(recipe.Ingedients);
        recipeIngedients.Add(IngredientType.Bun); // all burgers are topped with a bun

        if (recipeIngedients.Distinct().Count() != ingredients.Distinct().Count())
            return false;

        foreach (IngredientType ingredientType in  System.Enum.GetValues(typeof(IngredientType)))
        {
            if (recipeIngedients.Where(i => i == ingredientType).Count() != ingredients.Where(i => i == ingredientType).Count())
                return false;
        }

        return true;
    }

    void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player == null) return;
        Player = player;

        if (Player.transform.position.x < 0)
            Player.dropZone = PlayerController.DropOffZone.Left;
        else
            Player.dropZone = PlayerController.DropOffZone.Right;
    }

    void OnTriggerExit(Collider other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player == null) return;
        player.dropZone = PlayerController.DropOffZone.None;
        Player = null;
    }
}
