using System;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField] Text MoneyText;
    [SerializeField] Text TimeRemainingText;

    public void SetMoney(float money)
    {
        MoneyText.text = $"${money}";
    }

    public void SetTime(float seconds)
    {
        var ts = TimeSpan.FromSeconds(seconds);
        TimeRemainingText.text = ts.ToString("mm':'ss");
    }
}
