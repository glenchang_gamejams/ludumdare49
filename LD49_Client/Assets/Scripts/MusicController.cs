using UnityEngine;

public class MusicController : MonoBehaviour
{
    static MusicController Instance { get; set; }

    [SerializeField] AudioSource AudioSource;

    public static void SetVolume(float volume)
    {
        if (Instance != null)
            Instance.AudioSource.volume = volume;
    }

    public static float GetVolume()
    {
        return Instance?.AudioSource.volume ?? 1;
    }

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
