using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsMenuController : MonoBehaviour
{
    private const string MainMenu = "Menu";

    [SerializeField] Slider MusicSlider;

    private void Start()
    {
        MusicSlider.value = MusicController.GetVolume();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire3")) // B
        {
            OnClickBack();
        }
    }

    public void SetMusicVolume(float volume)
    {
        MusicController.SetVolume(volume);
    }

    public void OnClickBack()
    {
        SceneManager.LoadScene(MainMenu);
    }
}
