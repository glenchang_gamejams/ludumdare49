using UnityEngine;
using UnityEngine.SceneManagement;

public class Init : MonoBehaviour
{
    private const string MainMenu = "Menu";
    
    void Start()
    {
        SceneManager.LoadScene(MainMenu);
    }
}
