using UnityEngine;

public class TextBounce : MonoBehaviour
{
    [SerializeField][Range(0, 1)] float ScaleRange;
    [SerializeField] float ScaleSpeed;

    // Update is called once per frame
    void Update()
    {
        transform.localScale = Vector3.one * (1 + (Mathf.Sin(Time.time * ScaleSpeed) * ScaleRange));
    }
}
